<?php

namespace Korablikdev;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use Korablikdev\Messaging\Request\EmailRequest;
use Korablikdev\Messaging\Request\RequestInterface;
use Korablikdev\Messaging\Request\SmsRequest;
use Psr\Log\LoggerInterface;
use Korablikdev\Messaging\Enums\HttpCodes;
use Korablikdev\Messaging\Exceptions\ApiException;
use Korablikdev\Messaging\Exceptions\Api\ApiValidationException;
use Korablikdev\Messaging\Enums\AvailableProviders;


/**
 * Class ApiClient
 * @package Korablikdev
 */
class ApiClient
{
    const API_VERSION = 'v1';

    private $http_client;

    /**
     * ApiClient constructor.
     *
     * @param string               $baseApiUrl
     * @param LoggerInterface|null $logger
     * @param int                  $connectionTimeout
     */
    public function __construct(string $baseApiUrl, LoggerInterface $logger = null, $connectionTimeout = 30)
    {
        $this->http_client = new Client([
            //'timeout'         => 2.0,
            'allow_redirects' => true,
            'verify'          => false,
            //'debug'           => true,
            'base_uri'        => $baseApiUrl,
            'headers'         => [
                'Content-Type' => 'application/json',
                'Accept'       => 'application/json',
            ],
        ]);
    }


    /**
     * Отправка POST запроса
     *
     * @param string $method
     * @param array  $params
     *
     * @return \Psr\Http\Message\ResponseInterface
     * @throws ApiException
     * @throws ApiValidationException
     */
    public function post(string $method, array $params = [])
    {
        try {
            $response = $this->http_client->post($method, [
                'body' => json_encode($params),
            ]);
        } catch (RequestException $e) {

            if ($e->getResponse()->getStatusCode() === HttpCodes::HTTP_VALIDATION_ERROR) {
                $body = json_decode($e->getResponse()->getBody()->getContents(), true);
                if ( ! empty($body['errors']['children'])) {
                    throw new ApiValidationException(json_encode($body['errors']['children']));
                }
            }

            throw new ApiException($e->getMessage());
        }

        return $response;
    }


    /**
     * Отправка Email
     *
     * @param string $email
     * @param array  $params
     *
     * @return string|bool
     */
    public function email(string $email, array $params)
    {
        try {
            $response = $this->post('/api/sender', array_merge([
                'provider' => AvailableProviders::PROVIDER_EMAIL,
                'email'    => $email,
            ], $params));
        } catch (ApiException $e) {
            return $e->getMessage();
        }

        if ($response->getStatusCode() === HttpCodes::HTTP_CREATED) {
            return true;
        }

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * Отправка SMS
     *
     * @param string $phone
     * @param array  $params
     *
     * @return string|bool
     */
    public function sms(string $phone, array $params)
    {
        try {
            $response = $this->post('/api/sender', array_merge([
                'provider' => AvailableProviders::PROVIDER_SMS,
                'phone'    => $phone,
            ], $params));
        } catch (ApiException $e) {
            return $e->getMessage();
        }

        if ($response->getStatusCode() === HttpCodes::HTTP_CREATED) {
            return true;
        }

        return json_decode($response->getBody()->getContents(), true);
    }

    public function sendRequest(RequestInterface $request)
    {
        try {
            $response = $this->post('/api/sender', $request->getParams());
        } catch (ApiException $e) {
            return $e->getMessage();
        }

        if ($response->getStatusCode() === HttpCodes::HTTP_CREATED) {
            return true;
        }

        return json_decode($response->getBody()->getContents(), true);
    }


    /**
     * Отправка Viber сообщения
     *
     * @param string $phone
     * @param array  $params
     *
     * @return string|bool
     */
    public function viber(string $phone, array $params)
    {
        try {
            $response = $this->post('/api/sender', array_merge([
                'provider' => AvailableProviders::PROVIDER_VIBER,
                'phone'    => $phone,
            ], $params));
        } catch (ApiException $e) {
            return $e->getMessage();
        }

        if ($response->getStatusCode() === HttpCodes::HTTP_CREATED) {
            return true;
        }

        return json_decode($response->getBody()->getContents(), true);
    }
}