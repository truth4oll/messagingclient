<?php

namespace Korablikdev\Messaging\Enums;

/**
 * Class AvailableProviders
 * @package Korablik\Mailing\Api\Actions\Enums
 */
class AvailableProviders
{
    const PROVIDER_SMS = 'sms';
    const PROVIDER_EMAIL = 'email';
    const PROVIDER_VIBER = 'viber';
}