<?php

namespace Korablikdev\Messaging\Enums;

/**
 * Class DefinedTemplates
 * @package Korablik\Mailing\Api\Actions\Enums
 */
class DefinedTemplates
{
    const SMS_CONFIRM_CODE = 'sms_confirm_code';
    const SMS_NO_ANSWER = 'sms_no_answer';
    const SMS_FORGOT_PASSWORD_CODE = 'sms_forgot_password_code';
    const SMS_ASM_NOTIFY = 'sms_asm_notify';

    //смс пикапы
    const SMS_ORDER_PICKUP_NEW = 'sms_order_pickup_new';
    const SMS_ORDER_PICKUP_NEW_SHOP = 'sms_order_pickup_new_shop';
    const SMS_ORDER_PICKUP_CANCELED = 'sms_order_pickup_canceled';
    const SMS_ORDER_PICKUP_CANCELED_PAID = 'sms_order_pickup_canceled_paid';
    const SMS_ORDER_PICKUP_READY = 'sms_order_pickup_ready';
    const SMS_ORDER_PICKUP_PINCODE = 'sms_order_pickup_pincode';
    const SMS_ORDER_PICKUP_READY_PINCODE = 'sms_order_pickup_ready_pincode';

    //смс курьер
    const SMS_ORDER_COURIER_CANCELED = 'sms_order_courier_canceled';
    const SMS_ORDER_COURIER_DELIVERING = 'sms_order_courier_delivering';

    //емейлы пикап
    /** @var string заказ пикап - создан */
    const EMAIL_ORDER_PICKUP_CREATED = 'email_order_pickup_created';

    /** @var string заказ пикап - создан */
    const EMAIL_ORDER_PICKUP_CREATED_SHOP = 'email_order_pickup_created_shop';

    /** @var string заказ пикап - готов к выдаче */
    const EMAIL_ORDER_PICKUP_READY = 'email_order_pickup_ready';

    /** @var string заказ пикап - готов к выдаче, оплачен */
    const EMAIL_ORDER_PICKUP_READY_PAID = 'email_order_pickup_ready_paid';

    /** @var string заказ пикап - отменен */
    const EMAIL_ORDER_PICKUP_CANCELED = 'email_order_pickup_canceled';

    /** @var string заказ пикап - отменен оплаченый */
    const EMAIL_ORDER_PICKUP_CANCELED_PAID = 'email_order_pickup_canceled_paid';


    //емейлы курьер
    /** @var string заказ курьерская доставка - подтвержден */
    const EMAIL_ORDER_COURIER_CONFIRMED = 'email_order_courier_confirmed';

    /** @var string заказ курьерская доставка - доставляется */
    const EMAIL_ORDER_COURIER_DELIVERING = 'email_order_courier_delivering';

    /** @var string заказ курьерская доставка - отменен */
    const EMAIL_ORDER_COURIER_CANCELED = 'email_order_courier_canceled';

    /** @var string подтверждение емейла клиента при регистрации - отменен */
    const EMAIL_CLIENT_REGISTER_CONFIRM_EMAIL = 'email_client_register_confirm_email';

    /** @var string */
    const EMAIL_CLIENT_REGISTER_COMPLETED = 'email_client_register_completed';

    /** @var string восстановление пароля */
    const EMAIL_CLIENT_FORGOT_PASSWORD = 'email_client_forgot_password';

    /** @var string активация бонусной карты для незарегистрированного клиента */
    const EMAIL_CARD_ACTIVATE_UNREGISTERED = 'email_card_activate_unregistered';

    /** @var string активация бонусной карты для зарегистрированного клиента */
    const EMAIL_CARD_ACTIVATE_REGISTERED = 'email_card_activate_registered';

    /** @var string */
    const EMAIL_CARD_ACTIVATE_CONFIRM_EMAIL = 'email_card_activate_confirm_email';

    /** @var string */
    const EMAIL_PAYMENT_CANCEL = 'email_payment_cancel';

    /** @var string */
    const EMAIL_PAYMENT_CHANGE_ORDER_SUM = 'email_payment_change_order_sum';

    /** @var string  */
    const EMAIL_PAYMENT_FAILED = 'email_payment_failed';

    /** @var string  */
    const EMAIL_PAYMENT_SUCCESS = 'email_payment_success';
}
