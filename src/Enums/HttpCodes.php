<?php

namespace Korablikdev\Messaging\Enums;

/**
 * Class HttpCodes
 * @package Korablik\Messaging\Api\Enums
 */
class HttpCodes
{
    const HTTP_OK = 200;
    const HTTP_CREATED = 201;
    const HTTP_NO_CONTENT = 204;
    const HTTP_VALIDATION_ERROR = 400;
    const HTTP_CONFLICT = 409;
}