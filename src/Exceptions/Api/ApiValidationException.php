<?php

namespace Korablikdev\Messaging\Exceptions\Api;

use Korablikdev\Messaging\Exceptions\ApiException;


/**
 * Class ApiValidationException
 * @package Korablikdev\Messaging\Exceptions\Api
 */
class ApiValidationException extends ApiException
{

}