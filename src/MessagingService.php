<?php

namespace Korablikdev\Messaging\Service;

use Korablikdev\Messaging\ApiClient;
use Korablikdev\Messaging\Enums\DefinedTemplates;
use Korablikdev\Messaging\Exceptions\ApiException;
use Korablikdev\Messaging\Request\EmailRequest;
use Korablikdev\Messaging\Request\RequestInterface;
use Korablikdev\Messaging\Request\SmsRequest;
use Psr\Log\LoggerInterface;


/**
 * Class Service
 * @package Korablik\Mailing
 */
class MessagingService
{
    /**
     * @var ApiClient
     */
    private $client;

    /** @var LoggerInterface */
    private $logger;

    /** @var string адрес сайта, подставляется в письмах */
    private $site_url;

    /**
     * Service constructor.
     *
     * @param LoggerInterface|null $logger
     * @param array                $config
     * @param int                  $connectionTimeout
     *
     * @throws ApiException
     */
    public function __construct(LoggerInterface $logger = null, array $config, $connectionTimeout = 30)
    {
        if (empty($logger)) {
            throw new ApiException('Logger is not defined');
        }

        if (empty($config['base_api_url'])) {
            throw new ApiException('Base api url is not defined');
        }

        if (empty($config['site_url'])) {
            throw new ApiException('site_url is not defined');
        }

        $this->logger = $logger;
        $this->setSiteUrl($config['site_url']);
        $this->client = new ApiClient($config['base_api_url'], $logger, $connectionTimeout);

    }

    /**
     * @return string
     */
    public function getSiteUrl(): string
    {
        return $this->site_url;
    }

    /**
     * @param string $site_url
     */
    public function setSiteUrl(string $site_url)
    {
        $this->site_url = $site_url;
    }

    public function getLogger()
    {
        return $this->logger;
    }


    /**
     * @param RequestInterface $request
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function getResponse($request)
    {
        $response = $this->client->sendRequest($request);

        $requestName = (new \ReflectionClass($request))->getShortName();

        if ($response === true) {
            $this->getLogger()->info($requestName,
                [
                    'params' => $request->getParams()
                ]
            );

            return true;
        }

        $this->getLogger()->error($requestName,
            [
                'params'   => $request->getParams(),
                'response' => $response
            ]
        );

        $result = json_decode($response);

        foreach ($result as $row) {
            if ( ! empty($row->errors)) {
                throw new ApiException(array_shift($row->errors));
            }
        }

        return false;
    }


    /**
     * Отправка кода подтверждения
     *
     * @param string $phone
     * @param string $code
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendConfirmCode(string $phone, string $code)
    {
        $request = (new SmsRequest())
            ->setForced(1)
            ->setPhone($phone)
            ->setTemplate(DefinedTemplates::SMS_CONFIRM_CODE)
            ->setTemplateVars(['code' => $code]);

        return $this->getResponse($request);
    }

    /**
     * Отправка смс клиенту о недозвоне
     *
     * @param string $phone
     *
     * @param string $transactionId
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendSmsNoAnswer(string $phone, string $transactionId)
    {
        $request = (new SmsRequest())
            ->setPhone($phone)
            ->setTemplate(DefinedTemplates::SMS_NO_ANSWER)
            ->setTemplateVars([
                'transaction_id' => $transactionId
            ]);

        return $this->getResponse($request);
    }


    /**
     * CМС об отмене заказа
     *
     * @param string $phone
     * @param string $transactionId
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendSmsOrderPickupCanceled(string $phone, string $transactionId)
    {
        $request = (new SmsRequest())
            ->setPhone($phone)
            ->setTemplate(DefinedTemplates::SMS_ORDER_PICKUP_CANCELED)
            ->setTemplateVars([
                'transaction_id' => $transactionId
            ]);

        return $this->getResponse($request);
    }

    /**
     * СМС об отмене оплаченного заказа
     *
     * @param string $phone
     * @param string $transactionId
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendSmsOrderPickupCanceledPaid(string $phone, string $transactionId)
    {
        $request = (new SmsRequest())
            ->setPhone($phone)
            ->setTemplate(DefinedTemplates::SMS_ORDER_PICKUP_CANCELED_PAID)
            ->setTemplateVars([
                'transaction_id' => $transactionId
            ]);

        return $this->getResponse($request);
    }


    /**
     * Оформлен пикап, смс клиенту
     *
     * @param string $phone
     * @param string $transactionId
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendSmsOrderPickupNew(string $phone, string $transactionId)
    {
        $request = (new SmsRequest())
            ->setPhone($phone)
            ->setTemplate(DefinedTemplates::SMS_ORDER_PICKUP_NEW)
            ->setTemplateVars([
                'transaction_id' => $transactionId
            ]);

        return $this->getResponse($request);
    }

    /**
     * Оформлен пикап, смс магазину
     *
     * @param string $phone
     * @param string $transactionId
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendSmsOrderPickupNewShop(string $phone, string $transactionId)
    {
        $request = (new SmsRequest())
            ->setPhone($phone)
            ->setTemplate(DefinedTemplates::SMS_ORDER_PICKUP_NEW_SHOP)
            ->setTemplateVars([
                'transaction_id' => $transactionId
            ]);

        return $this->getResponse($request);

    }

    /**
     * @param string $phone
     * @param string $pinCode
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendSmsOrderPickupPincode(string $phone, string $pinCode)
    {
        $request = (new SmsRequest())
            ->setPhone($phone)
            ->setTemplate(DefinedTemplates::SMS_ORDER_PICKUP_PINCODE)
            ->setTemplateVars([
                'pin_code' => $pinCode,
            ]);

        return $this->getResponse($request);
    }


    /**
     * СМС о готовности пикапа
     *
     * @param string $phone
     *
     * @param string $transactionId
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendSmsOrderPickupReady(string $phone, string $transactionId)
    {
        $request = (new SmsRequest())
            ->setPhone($phone)
            ->setTemplate(DefinedTemplates::SMS_ORDER_PICKUP_READY)
            ->setTemplateVars([
                'transaction_id' => $transactionId
            ]);

        return $this->getResponse($request);
    }


    /**
     * СМС о готовности пикапа с пинкодом
     *
     * @param string $phone
     * @param string $transactionId
     * @param string $address
     * @param string $pin_code
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendSmsOrderPickupReadyPincode(
        string $phone,
        string $transactionId,
        string $address,
        string $pin_code
    ) {
        $request = (new SmsRequest())
            ->setPhone($phone)
            ->setTemplate(DefinedTemplates::SMS_ORDER_PICKUP_READY_PINCODE)
            ->setTemplateVars([
                'transaction_id' => $transactionId,
                'address'        => $address,
                'pin_code'       => $pin_code,
            ]);

        return $this->getResponse($request);
    }


    /**
     * Отправка сообщения клиенту об отмене заказа
     *
     * @param string $phone
     * @param string $transactionId
     *
     * @return bool
     * @throws \Exception
     */
    public function sendSmsOrderCourierCanceled(string $phone, string $transactionId): bool
    {
        $request = (new SmsRequest())
            ->setPhone($phone)
            ->setTemplate(DefinedTemplates::SMS_ORDER_COURIER_CANCELED)
            ->setTemplateVars([
                'transaction_id' => $transactionId,
            ]);

        return $this->getResponse($request);
    }

    /**
     * Отправка сообщения клиенту об отмене заказа
     *
     * @param string $phone
     * @param string $transactionId
     *
     * @return bool
     * @throws \Exception
     */
    public function sendSmsOrderCourierDelivering(string $phone, string $transactionId): bool
    {
        $request = (new SmsRequest())
            ->setPhone($phone)
            ->setTemplate(DefinedTemplates::SMS_ORDER_COURIER_DELIVERING)
            ->setTemplateVars([
                'transaction_id' => $transactionId,
            ]);

        return $this->getResponse($request);
    }


    /**
     * Отправки sms клиенту с кодов на восстановление пароля
     *
     * @param string $phone
     * @param string $code
     *
     * @return bool
     * @throws \Exception
     */
    public function sendForgotPasswordSms(string $phone, string $code): bool
    {
        $request = (new SmsRequest())
            ->setPhone($phone)
            ->setTemplate(DefinedTemplates::SMS_FORGOT_PASSWORD_CODE)
            ->setTemplateVars([
                'code' => $code,
            ]);

        return $this->getResponse($request);
    }





    //емейлы пикап


    /**
     * @param       $email
     * @param       $transactionId
     * @param array $params
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendEmailOrderPickupCreated($email, $transactionId, $params = [])
    {
        $request = (new EmailRequest())
            ->setEmail($email)
            ->setTemplate(DefinedTemplates::EMAIL_ORDER_PICKUP_CREATED)
            ->setTemplateVars([
                'transactionId' => $transactionId,
            ])->setSubjectVars([
                'transactionId' => $transactionId
            ]);

        return $this->getResponse($request);
    }

    /**
     * @param       $email
     * @param       $transactionId
     * @param array $params
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendEmailOrderPickupCreatedShop($email, $transactionId, $params = [])
    {
        $request = (new EmailRequest())
            ->setEmail($email)
            ->setTemplate(DefinedTemplates::EMAIL_ORDER_PICKUP_CREATED_SHOP)
            ->setTemplateVars([
                'transactionId' => $transactionId,
            ])->setSubjectVars([
                'transactionId' => $transactionId
            ]);

        return $this->getResponse($request);
    }

    /**
     * Письмо о готовности пикап заказа
     *
     * @param       $email
     * @param       $transactionId
     * @param array $params
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendEmailOrderPickupReady($email, $transactionId, $params = [])
    {
        $request = (new EmailRequest())
            ->setEmail($email)
            ->setTemplate(DefinedTemplates::EMAIL_ORDER_PICKUP_READY)
            ->setTemplateVars([
                'transactionId' => $transactionId,
            ])->setSubjectVars([
                'transactionId' => $transactionId
            ]);

        return $this->getResponse($request);
    }


    /**
     * Письмо о готовности оплаченного пикап заказа
     *
     * @param       $email
     *
     * @param       $transactionId
     * @param array $params
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendEmailOrderPickupReadyPaid($email, $transactionId, $params = [])
    {
        $request = (new EmailRequest())
            ->setEmail($email)
            ->setTemplate(DefinedTemplates::EMAIL_ORDER_PICKUP_READY_PAID)
            ->setTemplateVars([
                'transactionId' => $transactionId,
            ])->setSubjectVars([
                'servername'    => $this->getSiteUrl(),
                'transactionId' => $transactionId,
            ]);

        return $this->getResponse($request);
    }

    /**
     * Письмо об отмене пикап заказа
     *
     * @param       $email
     *
     * @param       $transactionId
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendEmailOrderPickupCanceled($email, $transactionId)
    {
        $request = (new EmailRequest())
            ->setEmail($email)
            ->setTemplate(DefinedTemplates::EMAIL_ORDER_PICKUP_CANCELED)
            ->setTemplateVars([
                'transactionId' => $transactionId,
            ])->setSubjectVars([
                'servername'    => $this->getSiteUrl(),
                'transactionId' => $transactionId,
            ]);

        return $this->getResponse($request);
    }

    /**
     * Письмо об отмене заказа доставки
     *
     * @param       $email
     *
     * @param       $transactionId
     * @param array $params
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendEmailOrderCourierCanceled($email, $transactionId, $params = [])
    {
        $request = (new EmailRequest())
            ->setEmail($email)
            ->setTemplate(DefinedTemplates::EMAIL_ORDER_COURIER_CANCELED)
            ->setTemplateVars([
                'transactionId' => $transactionId,
            ])->setSubjectVars([
                'transactionId' => $transactionId,
            ]);

        return $this->getResponse($request);
    }

    /**
     * Письмо об отмене оплаченного пикап заказа
     *
     * @param       $email
     *
     * @param       $transactionId
     * @param array $params
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendEmailOrderPickupCanceledPaid($email, $transactionId, $params = [])
    {
        $request = (new EmailRequest())
            ->setEmail($email)
            ->setTemplate(DefinedTemplates::EMAIL_ORDER_PICKUP_CANCELED_PAID)
            ->setTemplateVars([
                'transactionId' => $transactionId,
            ])->setSubjectVars([
                'servername'    => $this->getSiteUrl(),
                'transactionId' => $transactionId,
            ]);

        return $this->getResponse($request);
    }

    /**
     * Письмо об отмене оплаченного пикап заказа
     *
     * @param       $email
     *
     * @param       $transactionId
     * @param array $params
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendEmailOrderCourierCanceledPaid($email, $transactionId)
    {
        $request = (new EmailRequest())
            ->setEmail($email)
            ->setTemplate(DefinedTemplates::EMAIL_ORDER_PICKUP_CANCELED_PAID)
            ->setTemplateVars([
                'transactionId' => $transactionId,
            ])->setSubjectVars([
                'servername'    => $this->getSiteUrl(),
                'transactionId' => $transactionId,
            ]);

        return $this->getResponse($request);
    }


    /**
     * Письмо о подтвнерждении заказа доставки
     *
     * @param       $email
     *
     * @param       $transactionId
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendEmailOrderCourierConfirmed($email, $transactionId)
    {
        $request = (new EmailRequest())
            ->setEmail($email)
            ->setTemplate(DefinedTemplates::EMAIL_ORDER_COURIER_CONFIRMED)
            ->setTemplateVars([
                'transactionId' => $transactionId,
            ])->setSubjectVars([
                'servername'    => $this->getSiteUrl(),
                'transactionId' => $transactionId,
            ]);

        return $this->getResponse($request);
    }

    /**
     * @param       $email
     * @param       $transactionId
     * @param array $params
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendEmailOrderCourierDelivering($email, $transactionId)
    {
        $request = (new EmailRequest())
            ->setEmail($email)
            ->setTemplate(DefinedTemplates::EMAIL_ORDER_COURIER_DELIVERING)
            ->setTemplateVars([
                'transactionId' => $transactionId,
            ])->setSubjectVars([
                'servername'    => $this->getSiteUrl(),
                'transactionId' => $transactionId,
            ]);

        return $this->getResponse($request);
    }


    /**
     * Отправки email клиенту со ссылкой на восстановление пароля
     *
     * @param string $email
     * @param string $recovery_link ссылка на восстановление пароля https://...
     * @param array  $params
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendForgotPasswordEmail(string $email, string $recovery_link, $params = []): bool
    {
        $request = (new EmailRequest())
            ->setEmail($email)
            ->setTemplate(DefinedTemplates::EMAIL_CLIENT_FORGOT_PASSWORD)
            ->setTemplateVars(array_merge(['recovery_link' => $recovery_link], $params))
            ->setSubjectVars([]);

        return $this->getResponse($request);
    }


    /**
     * Отправки email клиенту со ссылкой на восстановление пароля
     *
     * @param string $email
     * @param array  $params
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendEmailClientRegisterConfirmEmail(string $email, $params = []): bool
    {
        $request = (new EmailRequest())
            ->setEmail($email)
            ->setTemplate(DefinedTemplates::EMAIL_CLIENT_REGISTER_CONFIRM_EMAIL)
            ->setTemplateVars($params)
            ->setSubjectVars([]);

        return $this->getResponse($request);
    }

    /**
     * Отправки email клиенту со ссылкой на восстановление пароля
     *
     * @param string $email
     * @param array  $params
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendEmailClientRegisterCompleted(string $email, $params = []): bool
    {
        $request = (new EmailRequest())
            ->setEmail($email)
            ->setTemplate(DefinedTemplates::EMAIL_CLIENT_REGISTER_COMPLETED)
            ->setTemplateVars($params)
            ->setSubjectVars([]);

        return $this->getResponse($request);
    }


    /**
     * @param       $email
     * @param       $card
     * @param array $params
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendEmailCardActivateRegistered($email, $card, $params = [])
    {
        $request = (new EmailRequest())
            ->setEmail($email)
            ->setTemplate(DefinedTemplates::EMAIL_CARD_ACTIVATE_REGISTERED)
            ->setTemplateVars(array_merge(['card' => $card], $params))
            ->setSubjectVars([]);

        return $this->getResponse($request);
    }

    /**
     * @param       $email
     * @param array $params
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendEmailCardActivateConfirmEmail($email, $params = [])
    {
        $request = (new EmailRequest())
            ->setEmail($email)
            ->setTemplate(DefinedTemplates::EMAIL_CARD_ACTIVATE_CONFIRM_EMAIL)
            ->setTemplateVars(array_merge($params))
            ->setSubjectVars([]);

        return $this->getResponse($request);
    }

    /**
     * @param string                    $phone
     * @param array                     $storeInfo
     * @param Korablik\Orm\Entity\Order $order
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendAsmNotification($phone, $storeInfo, $order)
    {
        if ( ! empty($phone)) {
            $request = (new SmsRequest())
                ->setPhone($phone)
                ->setTemplate(DefinedTemplates::SMS_CONFIRM_CODE)
                ->setTemplateVars([
                    'storeId'       => $storeInfo["XML_ID"],
                    'storeName'     => $storeInfo['TITLE'],
                    'transactionId' => $order->getTransactionId()
                ]);

            return $this->getResponse($request);
        }

        return false;
    }


    /**
     * @param $email
     * @param $transactionId
     * @param $pan
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendEmailPaymentCancel($email, $transactionId, $pan)
    {
        $request = (new EmailRequest())
            ->setEmail($email)
            ->setTemplate(DefinedTemplates::EMAIL_PAYMENT_CANCEL)
            ->setTemplateVars([
                'transaction_id' => $transactionId,
                'pan'            => $pan
            ])
            ->setSubjectVars([
                'transaction_id' => $transactionId,
            ]);

        return $this->getResponse($request);
    }


    /**
     * Отправить емейл об измннении суммы заказа
     *
     * @param       $email
     * @param       $transactionId
     * @param       $pan
     * @param       $diff
     *
     * @param array $params
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendEmailPaymentChangeOrderSum($email, $transactionId, $pan, $diff, $params = [])
    {
        $request = new EmailRequest();
        $request->setEmail($email)
            ->setTemplate(DefinedTemplates::EMAIL_PAYMENT_CHANGE_ORDER_SUM)
            ->setTemplateVars(array_merge($params, [
                        'transaction_id' => $transactionId,
                        'pan'            => $pan,
                        'diff'           => $diff
                    ]
                )
            )->setSubjectVars([
                'transaction_id' => $transactionId,
            ]);

        return $this->getResponse($request);
    }


    /**
     * @param       $email
     * @param       $transactionId
     * @param array $params
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendEmailPaymentFailed($email, $transactionId, $params = [])
    {
        $request = new EmailRequest();
        $request->setEmail($email)
            ->setTemplate(DefinedTemplates::EMAIL_PAYMENT_FAILED)
            ->setTemplateVars(array_merge(['transaction_id' => $transactionId], $params))
            ->setSubjectVars([
                'transaction_id' => $transactionId,
            ]);

        return $this->getResponse($request);
    }


    /**
     * @param         $email
     * @param         $transactionId
     * @param array   $params
     *
     * @return bool
     * @throws ApiException
     * @throws \ReflectionException
     */
    public function sendEmailPaymentSuccess($email, $transactionId, $params = [])
    {
        $request = new EmailRequest();
        $request->setEmail($email)
            ->setTemplate(DefinedTemplates::EMAIL_PAYMENT_SUCCESS)
            ->setTemplateVars(array_merge(['transaction_id' => $transactionId], $params))
            ->setSubjectVars([
                'transaction_id' => $transactionId,
            ]);

        return $this->getResponse($request);
    }


}
