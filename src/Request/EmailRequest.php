<?php

namespace Korablikdev\Messaging\Request;


use Korablikdev\Messaging\Enums\AvailableProviders;

/**
 * Class EmailRequest
 * @package Korablikdev\Messaging\Request
 */
class EmailRequest implements RequestInterface
{
    private $email;

    private $template;

    private $templateVars;

    private $subjectVars;

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     *
     * @return EmailRequest
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     *
     * @return EmailRequest
     */
    public function setTemplate(string $template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTemplateVars()
    {
        return $this->templateVars;
    }

    /**
     * @param mixed $templateVars
     *
     * @return EmailRequest
     */
    public function setTemplateVars(array $templateVars)
    {
        $this->templateVars = $templateVars;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubjectVars()
    {
        return $this->subjectVars;
    }

    /**
     * @param mixed $subjectVars
     *
     * @return EmailRequest
     */
    public function setSubjectVars($subjectVars)
    {
        $this->subjectVars = $subjectVars;

        return $this;
    }


    public function getParams()
    {
        return [
            'provider'     => AvailableProviders::PROVIDER_EMAIL,
            'email'        => $this->getEmail(),
            'template'     => $this->getTemplate(),
            'templateVars' => $this->getTemplateVars(),
            'subjectVars'  => $this->getSubjectVars(),
        ];
    }
}