<?php

namespace Korablikdev\Messaging\Request;

/**
 * Interface RequestInterface
 */
interface RequestInterface
{
    public function getParams();

}