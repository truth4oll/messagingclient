<?php

namespace Korablikdev\Messaging\Request;


use Korablikdev\Messaging\Enums\AvailableProviders;

/**
 * Class SmsRequest
 * @package Korablikdev\Messaging\Request
 */
class SmsRequest implements RequestInterface
{
    /**
     * @var integer принудительная отправка смс, не смотря на ограничение по времени
     */
    private $forced;

    /**
     * @var
     */
    private $phone;

    /**
     * @var string шаблон отправляемого смс
     */
    private $template;

    /**
     * @var array массив переменных для подставновки в шаблон
     */
    private $templateVars;

    /**
     * @return int
     */
    public function getForced(): int
    {
        return $this->forced;
    }

    /**
     * @param int $forced
     *
     * @return SmsRequest
     */
    public function setForced(int $forced)
    {
        $this->forced = $forced;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTemplate(): string
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     *
     * @return SmsRequest
     */
    public function setTemplate(string $template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTemplateVars(): array
    {
        return $this->templateVars;
    }

    /**
     * @param mixed $templateVars
     *
     * @return SmsRequest
     */
    public function setTemplateVars(array $templateVars)
    {
        $this->templateVars = $templateVars;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     *
     * @return SmsRequest
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }


    public function getParams()
    {
        return [
            'provider'     => AvailableProviders::PROVIDER_SMS,
            'phone'        => $this->getPhone(),
            'template'     => $this->getTemplate(),
            'templateVars' => $this->getTemplateVars(),
        ];
    }
}

